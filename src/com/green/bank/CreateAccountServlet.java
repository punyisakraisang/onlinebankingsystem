package com.green.bank;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.green.bank.model.CreateAccountModel;
import com.green.bank.model.CustomError;
import com.green.bank.util.AccountInvalidException;
import com.green.bank.util.ValidateUtil;

public class CreateAccountServlet extends HttpServlet {
	
	private static final long serialVersionUID = -5342463115794410191L;
	private static final String ERROR = "error";
	
	private final Logger logger = Logger.getLogger(CreateAccountServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("Enter CreateAccountServlet");
		
		if(!ValidateUtil.validateCreateAccountRequest(request)) {
			logger.error(CustomError.INVALID_PARAMETER);
			request.setAttribute(ERROR, CustomError.INVALID_PARAMETER.toString());
			goBack(request, response);
			return;
		}
		
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String address = request.getParameter("address");
		String city = request.getParameter("city");
		String branch = request.getParameter("branch");
		String zip = request.getParameter("zip");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("re_password");
		String phoneNumber = request.getParameter("phone");
		String email = request.getParameter("email");
		String accountType = request.getParameter("account_type");
		int amount = Integer.parseInt(request.getParameter("amount"));
		
		if (!password.equals(rePassword)) {
			logger.error(CustomError.CONFIRM_PASSWORD_NOT_MATCH);
			request.setAttribute(ERROR, CustomError.CONFIRM_PASSWORD_NOT_MATCH.toString());
			goBack(request, response);
			return;
		} 

		// Getting Current date
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String regDate = df.format(new Date());

		try {

			// Setting all variables to model class
			CreateAccountModel am = new CreateAccountModel(firstName, lastName, address, city, branch, zip, username, 
					rePassword, phoneNumber, email, accountType, regDate, amount);
			
			request.setAttribute("Account_details", am);
	        request.getRequestDispatcher("create_account_progress.jsp").forward(request, response);
			
		} catch (AccountInvalidException e) {
			logger.error("Error when create account: " + e.getMessage());
			request.setAttribute(ERROR, e.getMessage());
			goBack(request, response);
		}
	}
	
	private void goBack(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("create_account.jsp").forward(request, response);
	}

}
