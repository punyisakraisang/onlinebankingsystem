package com.green.bank.util;

import java.util.Optional;

public class LogUtil {
	
	private LogUtil() {}

	public static String maskPassword(Optional<String> password) {
		return password.isPresent()? "****" : "-";
	}

	public static String maskPassword(String password) {
		return !password.isEmpty() ? "****" : "-";
	}
}
