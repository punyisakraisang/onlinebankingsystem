package com.green.bank.util;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

public class MailUtil {
	
	private static Properties properties = System.getProperties();
	private static final Logger logger = Logger.getLogger(MailUtil.class);
	
	private static String username;
	private static String password;
	
	static {
		Properties p = new Properties();
		try {
			p.load(MailUtil.class.getResourceAsStream("/mail.properties"));
			properties.put("mail.smtp.host", p.getProperty("mail.smtp.host"));
	        properties.put("mail.smtp.port", p.getProperty("mail.smtp.port"));
	        properties.put("mail.smtp.ssl.enable", p.getProperty("mail.smtp.ssl.enable"));
	        properties.put("mail.smtp.auth", p.getProperty("mail.smtp.auth"));
			
	        username =  p.getProperty("mail.username");
	        password =  p.getProperty("mail.password");
	        
		} catch (IOException e) {
			logger.error("Unable to read the property file mail.properties" + e.getMessage());
		}
	}
	
	private MailUtil() {}
	
	public static void send(String recipient, String subject, String bodyText) {
		
		Session session = getSession();

        try {
            MimeMessage message = new MimeMessage(session);
            
            // Set message properties
            message.setFrom(new InternetAddress(username));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSubject(subject);
            message.setText(bodyText);

			logger.error("Sending email to " + recipient);
            Transport.send(message);
			logger.error("Sending email successfully to " + recipient);
			
        } catch (MessagingException e) {
			logger.error("Error when sending email to " + recipient + ": " + e.getMessage());
            e.printStackTrace();
        }
	}
	
	private static Session getSession() {
		return Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
	}
}
