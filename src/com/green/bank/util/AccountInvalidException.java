package com.green.bank.util;

public class AccountInvalidException extends Exception {
	
	private static final long serialVersionUID = -2915117255851027048L;

	public AccountInvalidException(String message) {
		super(message);
	}
}
