package com.green.bank.util;

public class ClientException extends Exception {

	private static final long serialVersionUID = 7627304691111385648L;

	public ClientException(String message) {
		super(message);
	}
}
