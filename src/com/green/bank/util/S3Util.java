package com.green.bank.util;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;

public class S3Util {
	
	public static final String BUCKET_NAME = "apolis-onlinebank-s3";

	private S3Util() {}
	
	public static AmazonS3 getClient() {
		return AmazonS3ClientBuilder.standard()
                .withRegion(Regions.US_WEST_1)
                .build();
	}
	
	public static ObjectMetadata getPdfMeta() {
		ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType("application/pdf");
        
        return metadata;
	}
}
