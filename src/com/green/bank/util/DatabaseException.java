package com.green.bank.util;

public class DatabaseException extends Exception {

	private static final long serialVersionUID = 8185728167588007112L;

	public DatabaseException(String message) {
		super(message);
	}
}
