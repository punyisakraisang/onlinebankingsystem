package com.green.bank.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

public class ValidateUtil {
	
	// Define parameter name that often used
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String ACCOUNT_NO = "account_no";
	private static final String FIRST_NAME = "first_name";
	private static final String LAST_NAME = "last_name";
	private static final String AMOUNT = "amount";
	
	private static final String INVALID_PARAM = "Invalid parameter: ";
	
	private static Logger logger = Logger.getLogger(ValidateUtil.class);
	
	private ValidateUtil() {}

	public static boolean validateCreateAccountRequest(HttpServletRequest req) {
		logger.info("Validate create account request");
		
		return validateString(req, FIRST_NAME, 2)
				&& validateString(req, LAST_NAME, 2)
				&& validateString(req, "address", 3)
				&& validateString(req, "city", 2)
				&& validateString(req, "branch", 2)
				&& validateString(req, "zip", 5)
				&& validateString(req, USERNAME, 5)
				&& validateString(req, PASSWORD, 5)
				&& validateString(req, "re_password", 5)
				&& validateString(req, "phone", 10)
				&& validateString(req, "email", 5)
				&& validateString(req, "account_type", 5)
				&& validateInt(req, AMOUNT);
	}
	
	public static boolean validateLoginRequest(HttpServletRequest req) {
		logger.info("Validate login request");
		
		return validateString(req, USERNAME, 5)
				&& validateString(req, PASSWORD, 0);
	}
	
	public static boolean validateDepositRequest(HttpServletRequest req) {
		logger.info("Validate deposit request");

		return validateString(req, ACCOUNT_NO, 5)
				&& validateString(req, USERNAME, 5)
				&& validateString(req, PASSWORD, 0)
				&& validateInt(req, AMOUNT);
	}
	
	public static boolean validateWithdrawRequest(HttpServletRequest req) {
		logger.info("Validate withdraw request");

		return validateString(req, ACCOUNT_NO, 5)
				&& validateString(req, USERNAME, 5)
				&& validateString(req, PASSWORD, 0)
				&& validateInt(req, AMOUNT);
	}
	
	
	public static boolean validateDepositSchemeRequest(HttpServletRequest req) {
		logger.info("Validate deposit scheme request");
		
		return validateString(req, ACCOUNT_NO, 5)
				&& validateInt(req, "year")
				&& validateInt(req, "interest_rate")
				&& validateInt(req, "deposit_amount")
				&& validateString(req, "value", 0);
	}
	
	public static boolean validateLoanRequest(HttpServletRequest req) {
		logger.info("Validate loan request");
		
		// validate normal parameter
		return validateString(req, ACCOUNT_NO, 5)
				&& validateInt(req, "loan_amount")
				&& validateString(req, FIRST_NAME, 0)
				&& validateString(req, LAST_NAME, 0)
				&& validateString(req, "address", 0)
				&& validateString(req, "email", 0)
				&& validateFile(req, "file");
	}
	
	public static boolean validateTransferRequest(HttpServletRequest req) {
		logger.info("Validate transfer request");
		
		return validateString(req, ACCOUNT_NO, 5)
				&& validateString(req, USERNAME, 5)
				&& validateString(req, PASSWORD, 0)
				&& validateString(req, "target_acc_no", 5)
				&& validateInt(req, AMOUNT);
	}
	
	private static boolean validateString(HttpServletRequest req, String paramName, int minLength) {
		String param = req.getParameter(paramName);
		
		if (param == null 
				|| param.matches("[\\n|\\r|\\t]") 
				|| param.length() < minLength) {
			logger.error(INVALID_PARAM + paramName);
			return false;
		}
		
		return true;
	}
	
	private static boolean validateInt(HttpServletRequest req, String paramName) {
		String param = req.getParameter(paramName);
		
		if (!NumberUtils.isCreatable(param)) {
			logger.error(INVALID_PARAM + paramName);
			return false;
		}
		
		try {
			Integer.parseInt(param);
			
		} catch (NumberFormatException e) {
			logger.error(INVALID_PARAM + paramName);
			return false;
		}
		
		return true;
	}
	
	private static boolean validateFile(HttpServletRequest req, String paramName) {
		
		if( !ServletFileUpload.isMultipartContent(req)) {
			logger.error(INVALID_PARAM + paramName);
			return false;
		}
		
		return true;
	}
}
