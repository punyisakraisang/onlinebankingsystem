package com.green.bank.model;

import java.io.InputStream;

public class FileModel {

	private InputStream fileContent;
	private String serviceName;
	private String firstName;
	private String lastName;
	private String extension;
	
	public FileModel(InputStream fileContent, String serviceName, String firstName, String lastName, String extension) {
		this.fileContent = fileContent;
		this.serviceName = serviceName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.extension = extension;
	}

	public InputStream getFileContent() {
		return fileContent;
	}

	public String getServiceName() {
		return serviceName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getExtension() {
		return extension;
	}
	
}
