package com.green.bank.model;

import java.io.Serializable;

public class DepositSchemeModel implements Serializable {
	private static final long serialVersionUID = 4435778761040393204L;
	private String accountNo;
	private String depositDate;
	private String value;
	private int year;
	private int interestRate;
	private int amount;
	
	public DepositSchemeModel(String accountNo, String depositDate, String value, int year, int interestRate,
			int amount) {
		this.accountNo = accountNo;
		this.depositDate = depositDate;
		this.value = value;
		this.year = year;
		this.interestRate = interestRate;
		this.amount = amount;
	}

	public String getAccountNo() {
		return accountNo;
	}
	
	public String getDepositDate() {
		return depositDate;
	}
	
	public String getValue() {
		return value;
	}
	
	public int getYear() {
		return year;
	}
	
	public int getInterestRate() {
		return interestRate;
	}
	
	public int getAmount() {
		return amount;
	}

}
