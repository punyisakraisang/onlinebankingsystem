package com.green.bank.model;

import java.io.Serializable;

public class GetLoanModel implements Serializable{
	
	private static final long serialVersionUID = -841108021829575495L;
	private String status;
	
	public GetLoanModel(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	
	@Override
	public String toString() {
		return "GetLoanModel [status:" + status + "]";
	}
	
}
