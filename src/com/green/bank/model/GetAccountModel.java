package com.green.bank.model;

import java.util.Optional;

import com.green.bank.util.LogUtil;

public class GetAccountModel {
	private Optional<String> accountNo;
	private Optional<String> username;
	private Optional<String> password;
	
	public GetAccountModel(String accountNo) {
		this.accountNo = Optional.of(accountNo);
		this.username = Optional.empty();
		this.password = Optional.empty();
	}
	
	public GetAccountModel(String username, String password) {
		this.accountNo = Optional.empty();
		this.username = Optional.of(username);
		this.password = Optional.of(password);
	}
	
	public GetAccountModel(String accountNo, String username, String password) {
		this.accountNo = Optional.of(accountNo);
		this.username = Optional.of(username);
		this.password = Optional.of(password);
	}

	public Optional<String> getAccountNo() {
		return accountNo;
	}

	public Optional<String> getUsername() {
		return username;
	}

	public Optional<String> getPassword() {
		return password;
	}
	
	@Override
	public String toString() {
		return "GetAccountModel [accountNo:"
				+ (accountNo.isPresent()? accountNo.get() : "-" )
				+ ", username:" + (username.isPresent()? username.get() : "-" ) 
				+ ", password:" + LogUtil.maskPassword(password) + "]";
	}
}
