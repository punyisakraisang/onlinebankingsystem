package com.green.bank.model;

public enum MailType {
	TRANSFER_SUCCESS, TRANSFER_FAIL;
}
