package com.green.bank.model;

import java.io.Serializable;

public class LoanModel implements Serializable{
	
	private static final long serialVersionUID = 4926087615487866675L;
	private String accountNo;
	private String status;
	private String firstName;
	private String lastName;
	private String email;
	private String address;
	private int loanAmount;
	private String documentUrl;
	
	public LoanModel(String accountNo, String status, String firstName, 
			String lastName, String email, String address, int loanAmount, String documentUrl) {
		this.accountNo = accountNo;
		this.status = status;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.loanAmount = loanAmount;
		this.documentUrl = documentUrl;
	}

	public String getAccountNo() {
		return accountNo;
	}
	
	public String getStatus() {
		return status;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getAddress() {
		return address;
	}
	
	public int getLoanAmount() {
		return loanAmount;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}
}
