package com.green.bank.model;

import java.io.Serializable;

import com.green.bank.util.LogUtil;

public class UpdateModel implements Serializable {

	private static final long serialVersionUID = -2257165371044678059L;
	private String accountNo;
	private String username;
	private String password;
	private int updateAmount;
	
	public UpdateModel(String accountNo, String username, String password, int updateAmount) {
		this.accountNo = accountNo;
		this.username = username;
		this.password = password;
		this.updateAmount = updateAmount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getUpdateAmount() {
		return updateAmount;
	} 
	
	@Override
	public String toString() {
		return "UpdateModel [accountNo" + accountNo
				+ ", username:" + username 
				+ ", password:" + LogUtil.maskPassword(password) 
				+ ", updateAmount:" + updateAmount + "]";
	}
}
