package com.green.bank.model;

import java.io.Serializable;

public class WithdrawAmountModel implements Serializable, UpdateAmountModel{
	
	private static final long serialVersionUID = -8206128918822204426L;
	private String accountNo;
	private int withdrawalAmount;
	
	public WithdrawAmountModel(String accountNo, int depositedAmount) {
		this.accountNo = accountNo;
		this.withdrawalAmount = depositedAmount;
	}

	@Override
	public String getAccountNo() {
		return accountNo;
	}

	@Override
	public int getUpdateAmount() {
		return withdrawalAmount*-1;
	}
	
	@Override
	public String toString() {
		return "WithdrawAmountModel [accountNo:" + accountNo
			+ ", withdrawalAmount:" + withdrawalAmount + "]";
	}
}
