package com.green.bank.model;

import java.io.Serializable;

public class TransferModel implements Serializable {

	private static final long serialVersionUID = 6105120906671896925L;
	private String accountNo;
	private String username;
	private String password;
	private String targetAccountNo;
	private int transferAmount;
	
	public TransferModel(String accountNo, String username, String password, String targetAccountNo,
			int transferAmount) {
		this.accountNo = accountNo;
		this.username = username;
		this.password = password;
		this.targetAccountNo = targetAccountNo;
		this.transferAmount = transferAmount;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getTargetAccountNo() {
		return targetAccountNo;
	}

	public int getTransferAmount() {
		return transferAmount;
	}
	
	@Override
	public String toString() {
		return "TransferModel [accountNo:" + accountNo
				+ ", targetAccountNo:" + targetAccountNo
				+ ", transferAmount:" + transferAmount +"]";
	}
	
}
