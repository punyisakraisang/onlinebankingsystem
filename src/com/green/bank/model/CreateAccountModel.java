package com.green.bank.model;

import java.io.Serializable;

import com.green.bank.util.AccountInvalidException;

public class CreateAccountModel implements Serializable {
	
	private static final long serialVersionUID = 2595965918461540620L;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String branch;
	private String zip;
	private String username;
	private String password;
	private String phoneNumber;
	private String email;
	private String accountType;
	private String regDate;
	private int amount;

	public CreateAccountModel(String firstName, String lastName, String address, String city, String branch,
			String zip, String username, String password, String phoneNumber, String email, String accountType,
			String regDate, int amount) throws AccountInvalidException {
		setFirstName(firstName);
		setLastName(lastName);
		setAddress(address);
		setCity(city);
		setBranch(branch);
		setZip(zip);
		setUsername(username);
		setPassword(password);
		setPhoneNumber(phoneNumber);
		setEmail(email);
		setAccountType(accountType);
		setRegDate(regDate);
		setAmount(amount);
	}

	public String getFirstName() {
		return firstName;
	}

	private void setFirstName(String firstName) throws AccountInvalidException {
		if (firstName == null || firstName.length() <= 2) {
			throw new AccountInvalidException("First name cannot be empty");
		}
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	private void setLastName(String lastName) throws AccountInvalidException {
		if (lastName == null || lastName.length() < 2) {
			throw new AccountInvalidException("Last name cannot be empty");
		}
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	private void setAddress(String address) throws AccountInvalidException {
		if (address == null || address.length() < 3) {
			throw new AccountInvalidException("Address cannot be empty, must be atleast 3 chars");
		}
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	private void setCity(String city) throws AccountInvalidException {
		if (city == null || city.length() < 2) {
			throw new AccountInvalidException("City cannot be empty, must be atleast 2 characters");
		}
		this.city = city;
	}

	public String getBranch() {
		return branch;
	}

	private void setBranch(String branch) throws AccountInvalidException {
		if (branch == null || branch.length() < 2) {
			throw new AccountInvalidException("Branch cannot be empty, must be atleast 2 characters");
		}
		this.branch = branch;
	}

	public String getZip() {
		return zip;
	}

	private void setZip(String zip) throws AccountInvalidException {
		if (zip == null || zip.length() < 5) {
			throw new AccountInvalidException("Zip cannot be empty, must be atleast 5 characters");
		}
		this.zip = zip;
	}

	public String getUsername() {
		return username;
	}

	private void setUsername(String username) throws AccountInvalidException {
		if (username == null || username.length() < 5) {
			throw new AccountInvalidException("Username cannot be empty, must be atleast 5 characters");
		}
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	private void setPassword(String password) throws AccountInvalidException {
		if (password == null || password.length() < 5) {
			// TODO matcher pattern for a regex: 1 special character, 1 numeric, 1 letter
			// atleast
			throw new AccountInvalidException("password cannot be empty, must be atleast 5 characters");
		}
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	private void setPhoneNumber(String phoneNumber) throws AccountInvalidException {
		if (phoneNumber == null || phoneNumber.length() < 10) {
			// TODO matcher pattern for a regex: only digits, atleast 10
			throw new AccountInvalidException("phone number cannot be empty, must be atleast 10 digits");
		}
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	private void setEmail(String email) throws AccountInvalidException {
		if (email == null || email.length() < 5) {
			// regex for @..
			throw new AccountInvalidException("email cannot be empty, must be atleast 5 characters");
		}
		this.email = email;
	}

	public String getAccountType() {
		return accountType;
	}

	private void setAccountType(String accountType) throws AccountInvalidException {
		if (accountType == null || accountType.length() < 5) {
			// regex for @..
			throw new AccountInvalidException("accountType cannot be empty, must be atleast 5 characters");
		}
		this.accountType = accountType; //TODO: change to AccountTypeEnum
	}

	public int getAmount() {
		return amount;
	}

	private void setAmount(int amount) throws AccountInvalidException {
		if (amount <= 0) {
			// regex for all digits
			throw new AccountInvalidException("Amount should be greater than 0");
		}
		this.amount = amount;
	}

	public String getRegDate() {
		return regDate;
	}

	private void setRegDate(String regDate) {
		this.regDate = regDate;
	}
}