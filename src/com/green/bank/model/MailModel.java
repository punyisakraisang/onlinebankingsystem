package com.green.bank.model;

import java.io.Serializable;

public class MailModel implements Serializable {

	private static final long serialVersionUID = -3203428167904860124L;
	private String recipient;
	private MailType type;
	private String errorMessage;
	private TransferModel model;

	public MailModel(String recipient, MailType type, TransferModel model) {
		this.recipient = recipient;
		this.type = type;
		this.errorMessage = "";
		this.model = model;
	}
	
	public MailModel(String recipient, MailType type, String errorMessage, TransferModel model) {
		this.recipient = recipient;
		this.type = type;
		this.errorMessage = errorMessage;
		this.model = model;
	}

	public String getRecipient() {
		return recipient;
	}

	public MailType getType() {
		return type;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
	public TransferModel getModel() {
		return model;
	}

	@Override
	public String toString() {
		return "MailModel [recipient:" + recipient
				+ ", type:" + type
				+ ", errorMessage:" + (!errorMessage.isEmpty()? errorMessage : "-") + "]";
	}
}
