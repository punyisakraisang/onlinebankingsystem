package com.green.bank.model;

public enum SqlError {
	UNIQUE_CONSTRAINT("23000");
	
	private String code;
	
	private SqlError(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
}
