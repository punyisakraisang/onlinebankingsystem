package com.green.bank.model;

public enum CustomError {
	INVALID_PARAMETER("401", "The request parameter is invalid"),
	INCORRECT_USERNAME_PASSWORD("402", "Username or password is incorrect"),
	INCORRECT_ACCOUNT_NUMBER("403", "Account number not found"),
	NOT_ENOUGH_BALANCE("404", "Not enough balance in the account"),
	USERNAME_ALREADY_EXIST("405", "Username alreay existed"),
	SAME_ACCOUNT_TRANSFER("406", "Cannot transfer to same account"),
	CONFIRM_PASSWORD_NOT_MATCH("407", "Password and confirm password is not matched");
	
	
	private String code;
	private String message;
	
	private CustomError(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	@Override
	public String toString() {
		return message + " [" + code + "]";
	}

}
