package com.green.bank.model;

import java.io.Serializable;

public class CreateDepositSchemeModel implements Serializable {
	
	private static final long serialVersionUID = -1510748509385025950L;
	private String accountNo;
	private String value;
	private int year;
	private int interestRate;
	private int amount;
	
	public CreateDepositSchemeModel(String accountNo, String value, int year, int interestRate, int amount) {
		this.accountNo = accountNo;
		this.value = value;
		this.year = year;
		this.interestRate = interestRate;
		this.amount = amount;
	}

	public String getAccountNo() {
		return accountNo;
	}
	
	public String getValue() {
		return value;
	}
	
	public int getYear() {
		return year;
	}
	
	public int getInterestRate() {
		return interestRate;
	}
	
	public int getAmount() {
		return amount;
	}
	
	@Override
	public String toString() {
		return "CreateDepositSchemeModel [accountNo:" + accountNo
				+ ", plan:" + value + "]";
	}

}
