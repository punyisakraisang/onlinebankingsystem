package com.green.bank.model;

public interface UpdateAmountModel{

	String getAccountNo();

	int getUpdateAmount();
	
	@Override
	String toString();
}
