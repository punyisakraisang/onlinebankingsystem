package com.green.bank.model;

import java.io.Serializable;

public class AccountModel implements Serializable {
	
	private static final long serialVersionUID = -4018229540427919964L;
	
	private String accountNo;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String branch;
	private String zip;
	private String username;
	private String password;
	private String phoneNumber;
	private String email;
	private String accountType;
	private String regDate;
	private int amount;

	public AccountModel(String accountNo, String firstName, String lastName, String address, String city, String branch,
			String zip, String username, String password, String phoneNumber, String email, String accountType,
			String regDate, int amount) {
		this.accountNo = accountNo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.branch = branch;
		this.zip = zip;
		this.username = username;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.accountType = accountType;
		this.regDate = regDate;
		this.amount = amount;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getBranch() {
		return branch;
	}

	public String getZip() {
		return zip;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() { // TODO: not return password to model?
		return password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public String getAccountType() {//TODO: change to AccountTypeEnum
		return accountType;
	}

	public int getAmount() {
		return amount;
	}

	public String getRegDate() {
		return regDate;
	}
}