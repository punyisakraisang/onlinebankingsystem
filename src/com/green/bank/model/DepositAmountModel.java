package com.green.bank.model;

import java.io.Serializable;

public class DepositAmountModel implements Serializable, UpdateAmountModel{

	private static final long serialVersionUID = 8792739979547189142L;

	private String accountNo;
	private int depositAmount;
	
	public DepositAmountModel(String accountNo, int depositedAmount) {
		this.accountNo = accountNo;
		this.depositAmount = depositedAmount;
	}

	@Override
	public String getAccountNo() {
		return accountNo;
	}

	@Override
	public int getUpdateAmount() {
		return depositAmount;
	}
	
	@Override
	public String toString() {
		return "DepositAmountModel [accountNo:" + accountNo
			+ ", depositAmount:" + depositAmount + "]";
	}
}
