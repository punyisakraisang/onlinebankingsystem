package com.green.bank;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.green.bank.model.CreateDepositSchemeModel;
import com.green.bank.model.CustomError;
import com.green.bank.model.DepositSchemeModel;
import com.green.bank.service.DepositSchemeService;
import com.green.bank.service.DepositSchemeServiceInterface;
import com.green.bank.util.DatabaseException;
import com.green.bank.util.ValidateUtil;

public class DepositSchemeServlet extends HttpServlet {

	private static final long serialVersionUID = -3153355383448883695L;

	private final DepositSchemeServiceInterface service = new DepositSchemeService();
	
	private final Logger logger = Logger.getLogger(DepositSchemeServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		logger.info("Enter DepositSchemeServlet");
		
		if(!ValidateUtil.validateDepositSchemeRequest(request)) {
			logger.error(CustomError.INVALID_PARAMETER);
			request.setAttribute("error", CustomError.INVALID_PARAMETER.toString());
			goBack(request, response);
			return;
		}
		
		// Get request parameter
		String accountNo = request.getParameter("account_no");
		int year = Integer.parseInt(request.getParameter("year"));
		int interestRate = Integer.parseInt(request.getParameter("interest_rate"));
		int depositAmount = Integer.parseInt(request.getParameter("deposit_amount"));
		String value = request.getParameter("value");

		try {
			// Create deposit scheme
			CreateDepositSchemeModel createModel = new CreateDepositSchemeModel(accountNo, value, year, interestRate, depositAmount);
			logger.info("Create deposit scheme: " + createModel);
			DepositSchemeModel model = service.createDepositScheme(createModel);
			
			// Save to session
			logger.info("Create deposit scheme success for account: " + accountNo);
			request.setAttribute("DepositScheme", model);
			request.getRequestDispatcher("deposit_scheme_progress.jsp").forward(request, response);

		} catch (DatabaseException e) {
			logger.error("Error when create deposit scheme: " + e.getMessage());
			request.setAttribute("error", e.getMessage());
			goBack(request, response);
		}
		
	}
	
	private void goBack(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("single_deposit_scheme.jsp").forward(request, response);
	}

}
