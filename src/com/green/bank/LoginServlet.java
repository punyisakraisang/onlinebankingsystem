package com.green.bank;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.green.bank.model.AccountModel;
import com.green.bank.model.CustomError;
import com.green.bank.model.GetAccountModel;
import com.green.bank.service.AccountService;
import com.green.bank.service.AccountServiceInterface;
import com.green.bank.util.DatabaseException;
import com.green.bank.util.ValidateUtil;

public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 2178865317863049636L;
	
	private final AccountServiceInterface service = new AccountService();
	
	private final Logger logger = Logger.getLogger(LoginServlet.class);


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("Enter LoginServlet");
		
		if(!ValidateUtil.validateLoginRequest(request)) {
			logger.error(CustomError.INVALID_PARAMETER);
			request.setAttribute("error", CustomError.INVALID_PARAMETER.toString());
			goBack(request, response);
			return;
		}
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		try {
			GetAccountModel getModel = new GetAccountModel(username, password);
			
			logger.info("Retrieve account information: " + getModel);
			AccountModel am = service.getAccount(getModel);
			
			// Save current account details in session
			request.getSession().setAttribute("userDetails", am);

			logger.info("Login success for username: " + username);
			RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
			
		} catch (DatabaseException e) {
			logger.error("Error when logging in: " + e.getMessage());
			request.setAttribute("error", e.getMessage());
			goBack(request, response);
		}
	}

	private void goBack(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}
}
