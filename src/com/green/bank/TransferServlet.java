package com.green.bank;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.green.bank.model.AccountModel;
import com.green.bank.model.CustomError;
import com.green.bank.model.TransferModel;
import com.green.bank.service.AccountService;
import com.green.bank.service.AccountServiceInterface;
import com.green.bank.util.DatabaseException;
import com.green.bank.util.ValidateUtil;

public class TransferServlet extends HttpServlet {
	
	private static final long serialVersionUID = 5668774863097279926L;
	private static final String ERROR = "error";
	
	private final AccountServiceInterface accountService = new AccountService();
	
	private final Logger logger = Logger.getLogger(TransferServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		logger.info("Enter TransferServlet");
		
		if(!ValidateUtil.validateTransferRequest(request)) {
			logger.error(CustomError.INVALID_PARAMETER);
			request.setAttribute(ERROR, CustomError.INVALID_PARAMETER.toString());
			goBack(request, response);
			return;
		}

		String accountNo = request.getParameter("account_no");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String targetAccountNo = request.getParameter("target_acc_no");
		int transferAmount = Integer.parseInt(request.getParameter("amount")); 
		
		// Cannot transfer to same account
		if (accountNo.equals(targetAccountNo)) {
			logger.error(CustomError.SAME_ACCOUNT_TRANSFER);
			request.setAttribute(ERROR, CustomError.SAME_ACCOUNT_TRANSFER.toString());
			goBack(request, response);
			return;
		}
		
		try {
			TransferModel transfer = new TransferModel(accountNo, username, password, targetAccountNo, transferAmount);

			logger.info("Transfer from account: " + accountNo + " to account:" + targetAccountNo);
			AccountModel model = accountService.transfer(transfer);
			
			// Save current account details in session
			request.getSession().setAttribute("userDetails", model);
			
			logger.info("Transfer success for account: " + accountNo);
			request.getRequestDispatcher("transfer_process.jsp").forward(request, response);

		} catch (DatabaseException e) {
			logger.error("Error when transfer: " + e.getMessage());
			request.setAttribute(ERROR, e.getMessage());
			goBack(request, response);
		}
	}
	
	private void goBack(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("transfer.jsp").forward(request, response);
	}
}
