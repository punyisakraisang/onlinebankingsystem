package com.green.bank;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.green.bank.model.AccountModel;
import com.green.bank.model.CustomError;
import com.green.bank.model.UpdateModel;
import com.green.bank.service.AccountService;
import com.green.bank.service.AccountServiceInterface;
import com.green.bank.util.DatabaseException;
import com.green.bank.util.ValidateUtil;

public class DepositServlet extends HttpServlet {

	private static final long serialVersionUID = 3160848446291840070L;
	
	private final AccountServiceInterface service = new AccountService();
	
	private final Logger logger = Logger.getLogger(DepositServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("Enter DepositServlet");
		
		if(!ValidateUtil.validateDepositRequest(request)) {
			logger.error(CustomError.INVALID_PARAMETER);
			request.setAttribute("error", CustomError.INVALID_PARAMETER.toString());
			goBack(request, response);
			return;
		}
		
		// Get request parameter
		String accountNo = request.getParameter("account_no");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		int depositAmount = Integer.parseInt(request.getParameter("amount")); 

		try {
			UpdateModel updateModel = new UpdateModel(accountNo, username, password, depositAmount);

			logger.info("Deposit to account: " + accountNo);
			AccountModel model = service.deposit(updateModel);
			
			// Save current account details in session
			request.getSession().setAttribute("userDetails", model);
			
			logger.info("Deposit success for account: " + accountNo);
			request.getRequestDispatcher("Deposit_process.jsp").forward(request, response);
			
		} catch (DatabaseException e) {
			logger.error("Error when deposit amount: " + e.getMessage());
			request.setAttribute("error", e.getMessage());
			goBack(request, response);
		}
		
	}
	
	private void goBack(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("deposit.jsp").forward(request, response);
	}

}
