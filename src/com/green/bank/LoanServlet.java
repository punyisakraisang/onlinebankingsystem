package com.green.bank;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.green.bank.model.CreateLoanModel;
import com.green.bank.model.CustomError;
import com.green.bank.model.FileModel;
import com.green.bank.model.LoanModel;
import com.green.bank.service.FileService;
import com.green.bank.service.FileServiceInterface;
import com.green.bank.service.LoanService;
import com.green.bank.service.LoanServiceInterface;
import com.green.bank.util.ClientException;
import com.green.bank.util.ValidateUtil;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,
				maxFileSize = 1024 * 1024 * 50,
				maxRequestSize = 1024 * 1024 * 100)
public class LoanServlet extends HttpServlet {

	private static final long serialVersionUID = 7923113276733280393L;
	private static final String SERVICE_NAME = "loan";
	
	private final LoanServiceInterface loanService = new LoanService();
	private final FileServiceInterface fileService = new FileService();
	
	private final Logger logger = Logger.getLogger(LoanServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		logger.info("Enter LoanServlet");
		
		if(!ValidateUtil.validateLoanRequest(request)) {
			logger.error(CustomError.INVALID_PARAMETER);
			request.setAttribute("error", CustomError.INVALID_PARAMETER.toString());
			goNext(request, response);
			return;
		}
		
		String accountNo = request.getParameter("account_no");
		int loanAmount = Integer.parseInt(request.getParameter("loan_amount"));
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String address = request.getParameter("address");
		String email = request.getParameter("email");
		Part filePart = request.getPart("loan_file");

		try {
			logger.info("Upload document to cloud before create loan");
			String url = uploadFile(filePart, firstName, lastName);
			
			CreateLoanModel createModel = new CreateLoanModel(accountNo, firstName, lastName, email, address, loanAmount, url);
			
			logger.info("Create loan for account: " + accountNo);
			LoanModel model =  loanService.createLoan(createModel);

			logger.info("Create loan success for account: " + accountNo);
			request.setAttribute("loanDetails", model);
			goNext(request, response);

		} catch (Exception e) {
			logger.error("Error when create loan: " + e.getMessage());
			request.setAttribute("error", e.getMessage());
			goNext(request, response);
		}
	}
	
	private String uploadFile(Part filePart, String firstName, String lastName) throws IOException, ClientException {
		
		// Prepare request to upload file
		String partName = filePart.getSubmittedFileName();
		String[] names = partName.trim().split("\\.");
		String ext = names[1];
	    InputStream fileContent = filePart.getInputStream();
	    
	    // Construct request
	    FileModel model = new FileModel(fileContent, SERVICE_NAME, firstName, lastName, ext);
	    return fileService.upload(model);
	}

	private void goNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("loan_process.jsp").forward(request, response);
	}

}
