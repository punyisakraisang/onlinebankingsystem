package com.green.bank.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import org.apache.log4j.Logger;

import com.green.bank.model.AccountModel;
import com.green.bank.model.CreateAccountModel;
import com.green.bank.model.CustomError;
import com.green.bank.model.DepositAmountModel;
import com.green.bank.model.GetAccountModel;
import com.green.bank.model.SqlError;
import com.green.bank.model.UpdateAmountModel;
import com.green.bank.model.WithdrawAmountModel;
import com.green.bank.util.DatabaseException;

public class AccountRepository implements AccountRepositoryInterface {
	
	private Logger logger = Logger.getLogger(AccountRepository.class);

	public AccountModel getAccount(GetAccountModel getModel) throws DatabaseException {
		logger.info("Enter account repository to get account: " + getModel);
		
		// Create statement string
		AccountModel model = null;
		String statementStr = 
				"select       ac.id, ac.f_name, ac.l_name, ac.address, ac.city, ac.branch, ac.zip"
				+ "           , ac.username, ac.password, ac.phone, ac.email, ac.account_type, ac.reg_date "
				+ "           , am.amount "
				+ "from       account ac " 
				+ "inner join amount am on am.id = ac.id "
				+ "where" + getWhereClauseForGetAccount(getModel);
		
		Optional<String> optAccountNo = getModel.getAccountNo();
		Optional<String> optUsername = getModel.getUsername();
		Optional<String> optPassword = getModel.getPassword();
		
		try (	Connection con = JdbcConnect.getConnection();
				PreparedStatement selectStatement = con.prepareStatement(statementStr); ) {

			// Set argument for get account
			if (optUsername.isPresent() && optPassword.isPresent()) {
				// Username and password are provided
				selectStatement.setString(1, optUsername.get());
				selectStatement.setString(2, optPassword.get());
				
				// Account number also provided
				if (optAccountNo.isPresent()) {
					selectStatement.setString(3, optAccountNo.get());
				}
				
			} else if (optAccountNo.isPresent()) {
				// Only account number is provided
				selectStatement.setString(1, optAccountNo.get());
				
			} else {
				// This should not happen
				String err = CustomError.INVALID_PARAMETER.toString();
				logger.error(err);
				throw new DatabaseException(err);
			}
			
			// Execute query
			logger.info("Get account: " + getModel);
			ResultSet accountResult = selectStatement.executeQuery();
			
			if (!accountResult.isBeforeFirst()) {
				String err;
				if (optUsername.isPresent() && optPassword.isPresent()) {
					err = CustomError.INCORRECT_USERNAME_PASSWORD.toString();
				} else {
					err = CustomError.INCORRECT_ACCOUNT_NUMBER.toString();
				}
				logger.error(err);
				throw new DatabaseException(err);
			} 

			// Get account information
			accountResult.next();
			String accountNo = accountResult.getString("id");
			String firstName = accountResult.getString("f_name");
			String lastName = accountResult.getString("l_name");
			String address = accountResult.getString("address");
			String city = accountResult.getString("city");
			String branch = accountResult.getString("branch");
			String zip = accountResult.getString("zip");
			String username = accountResult.getString("username");
			String password = accountResult.getString("password");
			String phoneNumber = accountResult.getString("phone");
			String email = accountResult.getString("email");
			String accountType = accountResult.getString("account_type");
			String regDate = accountResult.getString("reg_date");
			int amount = accountResult.getInt("amount");
			accountResult.close();

			model= new AccountModel(accountNo, firstName, lastName, address, city, branch, 
					zip, username, password, phoneNumber,  email, accountType, regDate, amount);
			
		} catch (SQLException e) {
			String err = "Error when get account: " + e.getMessage();
			logger.error(err);
			throw new DatabaseException(err);
		} 
		
		logger.info("Get account success for request " + getModel);
		return model;
	}
	
	// TODO make transactional
	public AccountModel createAccount(CreateAccountModel model) throws DatabaseException {
		logger.info("Enter account repository to create account");
		
		int acRow = -1;
		int amRow = -1;

		// Create account number
		String accountNo = generateAccountNo(model.getFirstName(), model.getLastName());

		// Create statement string
		String accountStatementStr = "insert into account" 
				+ "(id,f_name,l_name,address,city,branch,zip,username,password,phone,email,account_type,reg_date)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String amountStatementStr = "insert into amount(id,amount) values(?,?)";
		
		try (	Connection con = JdbcConnect.getConnection();
				PreparedStatement accountStatement = con.prepareStatement(accountStatementStr);
				PreparedStatement amountStatement = con.prepareStatement(amountStatementStr); ) {
			
			accountStatement.setString(1, accountNo);
			accountStatement.setString(2, model.getFirstName());
			accountStatement.setString(3, model.getLastName());
			accountStatement.setString(4, model.getAddress());
			accountStatement.setString(5, model.getCity());
			accountStatement.setString(6, model.getBranch());
			accountStatement.setString(7, model.getZip());
			accountStatement.setString(8, model.getUsername());
			accountStatement.setString(9, model.getPassword());
			accountStatement.setString(10, model.getPhoneNumber());
			accountStatement.setString(11, model.getEmail());
			accountStatement.setString(12, model.getAccountType());
			accountStatement.setString(13, model.getRegDate());
			
			amountStatement.setString(1, accountNo);
			amountStatement.setInt(2, model.getAmount());
			
			logger.info("Create account for account number: " + accountNo);
			acRow = accountStatement.executeUpdate();
			
			logger.info("Create amount for account number: " + accountNo);
			amRow = amountStatement.executeUpdate();
			
		}	catch (SQLException e) {
			String err;
			if(e.getSQLState().equals(SqlError.UNIQUE_CONSTRAINT.getCode())) {
				err = CustomError.USERNAME_ALREADY_EXIST.toString();
				
			} else {
				err = "Error when insert account: " + e.getMessage();
			}
			
			logger.error(err);
			throw new DatabaseException(err);
		}
		
		if (amRow != 1 && acRow != 1) {
			String err = "Inserted row is more than one: amount=" + amRow + " account=" + acRow;
			logger.error(err);
			throw new DatabaseException(err);
		}
		
		logger.info("Create account success for account number: " + accountNo);
		return parse(model, accountNo);
	}
	
	public AccountModel deposit(DepositAmountModel deposit) throws DatabaseException { 
		logger.info("Enter account repository to deposit amount: " + deposit);

		AccountModel updatedModel = update(deposit);
		
		logger.info("Deposit success for: " + deposit);
		return updatedModel;
	}
	
	public AccountModel withdraw(WithdrawAmountModel withdraw) throws DatabaseException { 
		logger.info("Enter account repository to withdraw amount: " + withdraw);

		AccountModel updatedModel = update(withdraw);
		
		logger.info("Withdraw success for: " + withdraw);
		return updatedModel;
	}
	
	// TODO: transactional
	private AccountModel update(UpdateAmountModel update) throws DatabaseException { 
		logger.info("Process update request: " + update);

		// Create statement string
		String accountNo = update.getAccountNo();
		String getStatementStr = "select id, amount from amount where id = ?";
		String updateStatementStr = "update amount set amount = ? where id = ?";
		
		try (	Connection con = JdbcConnect.getConnection();
				PreparedStatement getStatement = con.prepareStatement(getStatementStr);
				PreparedStatement updateStatement = con.prepareStatement(updateStatementStr);) {

			int row = -1;
			logger.info("Get amount of account number: " + accountNo);
			getStatement.setString(1, accountNo);
			
			synchronized(accountNo.intern()) {

				// Get latest amount before update
				ResultSet getResult = getStatement.executeQuery();
				
				if (!getResult.next()) {
					String err = "Cannot get amount of account number: " + accountNo;
					logger.error(err);
					throw new DatabaseException(err);
				} 
				
				// Calculate new updated amount
				int currentAmount = getResult.getInt("amount");
				getResult.close();
				
				if (currentAmount + update.getUpdateAmount() < 0) {
					String err = CustomError.NOT_ENOUGH_BALANCE.toString();
					logger.error(err + ": " + accountNo);
					throw new DatabaseException(err);
				}
				
				int updatedAmount = currentAmount + update.getUpdateAmount();
				
				// Update amount
				logger.info("Update amount of account number: " + accountNo);
				updateStatement.setInt(1, updatedAmount);
				updateStatement.setString(2, accountNo);
				row = updateStatement.executeUpdate();

			}
			
			if (row != 1) {
				String err = "Updated row is more than one: " + update;
				logger.error(err);
				throw new DatabaseException(err);
			}
			
		} catch (SQLException e) {
			String err = "Error when update amount: " + e.getMessage();
			logger.error(err);
			throw new DatabaseException(err);
		}
		
		logger.info("Update success for: " + update);
		
		logger.info("Get updated account model: " + accountNo);
		GetAccountModel getModel = new GetAccountModel(accountNo);
		return getAccount(getModel);
	}
	
	private String generateAccountNo(String firstName, String lastName) throws DatabaseException {
		logger.info("Create account number");
		int seq = -1;
		
		try ( 	Connection con = JdbcConnect.getConnection();
				Statement s = con.createStatement();){
			
			ResultSet result = s.executeQuery("select seq_account_no.nextval from dual");

			result.next();
			seq = result.getInt(1);
			result.close();
			
		} catch (SQLException e) {
			String err = "Error when generate account number: " + e.getMessage();
			logger.error(err);
			throw new DatabaseException(err);
		}
		
		return firstName.substring(0, 2)
				+ lastName.substring(0, 2)
				+ seq;
	}
	
	private String getWhereClauseForGetAccount(GetAccountModel getModel) {
		StringBuilder where = new StringBuilder();
		
		if (getModel.getUsername().isPresent() && getModel.getPassword().isPresent()) {
			where.append(" ac.username = ? and ac.password = ?");
			
			if (getModel.getAccountNo().isPresent()) {
				where.append(" and ");
			}
		}
		
		if (getModel.getAccountNo().isPresent()) {
			where.append(" ac.id = ?");
		}
		
		return where.toString();
	}
	
	private AccountModel parse(CreateAccountModel createModel, String accountNo) {
		return new AccountModel(
				accountNo, 
				createModel.getFirstName(), 
				createModel.getLastName(), 
				createModel.getAddress(), 
				createModel.getCity(), 
				createModel.getBranch(), 
				createModel.getZip(), 
				createModel.getUsername(), 
				createModel.getPassword(), 
				createModel.getPhoneNumber(), 
				createModel.getEmail(), 
				createModel.getAccountType(), 
				createModel.getRegDate(), 
				createModel.getAmount());
	}

}
