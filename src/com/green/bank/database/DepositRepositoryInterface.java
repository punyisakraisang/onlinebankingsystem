package com.green.bank.database;

import com.green.bank.model.CreateDepositSchemeModel;
import com.green.bank.model.DepositSchemeModel;
import com.green.bank.util.DatabaseException;

public interface DepositRepositoryInterface {
	
	DepositSchemeModel createDepositScheme(CreateDepositSchemeModel createModel) throws DatabaseException;
}
