package com.green.bank.database;

import com.green.bank.model.AccountModel;
import com.green.bank.model.CreateAccountModel;
import com.green.bank.model.DepositAmountModel;
import com.green.bank.model.GetAccountModel;
import com.green.bank.model.WithdrawAmountModel;
import com.green.bank.util.DatabaseException;

public interface AccountRepositoryInterface {

	AccountModel createAccount(CreateAccountModel model) throws DatabaseException;
	
	AccountModel getAccount(GetAccountModel getModel) throws DatabaseException;
	
	AccountModel deposit(DepositAmountModel deposit) throws DatabaseException;
	
	AccountModel withdraw(WithdrawAmountModel withdraw) throws DatabaseException;
}
