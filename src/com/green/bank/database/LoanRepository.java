package com.green.bank.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.green.bank.model.CreateLoanModel;
import com.green.bank.model.GetLoanModel;
import com.green.bank.model.LoanModel;
import com.green.bank.util.DatabaseException;

public class LoanRepository implements LoanRepositoryInterface {
	
	private Logger logger = Logger.getLogger(LoanRepository.class);
	private String pendingStatus = "pending";
	
	// TODO make transactional
	public LoanModel createLoan(CreateLoanModel createModel) throws DatabaseException {
		logger.info("Enter loan repository to create loan: " + createModel);

		String accountNo = createModel.getAccountNo();
		
		// Create statement string
		String insertStatementStr = "insert into loan " 
				+ "(id, amount, status, first_name, last_name, address, email, document_url)"
				+ "values(?, ?, ?, ?, ?, ?, ?, ?)";
		
		try (	Connection con = JdbcConnect.getConnection();
				PreparedStatement insertStatement = con.prepareStatement(insertStatementStr); ) {
			
			insertStatement.setString(1, accountNo);
			insertStatement.setInt(2, createModel.getLoanAmount());
			insertStatement.setString(3, pendingStatus);
			insertStatement.setString(4, createModel.getFirstName());
			insertStatement.setString(5, createModel.getLastName());
			insertStatement.setString(6, createModel.getAddress());
			insertStatement.setString(7, createModel.getEmail());
			insertStatement.setString(8, createModel.getDocumentUrl());
			
			logger.info("Create loan for account number: " + accountNo);
			int row = insertStatement.executeUpdate();
			
			if (row != 1) {
				String err = "Inserted row is more than one";
				logger.error(err);
				throw new DatabaseException(err);
			}
			
		}	catch (SQLException e) {
			String err = "Error when insert loan: " + e.getMessage();
			logger.error(err);
			throw new DatabaseException(err);
		}
		
		logger.info("Create loan success for account number: " + createModel);
		return parse(createModel, pendingStatus);
	}
	
	public List<LoanModel> getLoanList(GetLoanModel getModel) throws DatabaseException {
		logger.info("Enter loan repository to get loan: " + getModel);
		
		List<LoanModel> loanList = new ArrayList<>();
		
		
		// Create statement string
		String selectStatementStr = "select id, amount, status, first_name, last_name, address, email, document_url "
				+ "from loan "
				+ "where status = ?";
		
		try (	Connection con = JdbcConnect.getConnection();
				PreparedStatement selectStatement = con.prepareStatement(selectStatementStr); ) {
			
			selectStatement.setString(1, getModel.getStatus());
			
			logger.info("Get loan: " + getModel);
			ResultSet result = selectStatement.executeQuery();
			
			// Get result
			while (result.next()) {
				LoanModel loan = new LoanModel(
						result.getString("id"), 
						result.getString("status"),
						result.getString("first_name"),
						result.getString("last_name"),
						result.getString("email"),
						result.getString("address"),
						result.getInt("amount"),
						result.getString("document_url"));

				loanList.add(loan);
			}
			
		}	catch (SQLException e) {
			String err = "Error when get loan: " + e.getMessage();
			logger.error(err);
			throw new DatabaseException(err);
		}
		
		logger.info("Get loan success: " + getModel);
		return loanList;
	}
	
	private LoanModel parse(CreateLoanModel createModel, String status) {
		return new LoanModel(
				createModel.getAccountNo(), 
				status, 
				createModel.getFirstName(), 
				createModel.getLastName(),
				createModel.getEmail(), 
				createModel.getAddress(), 
				createModel.getLoanAmount(), 
				createModel.getDocumentUrl());
	}

}
