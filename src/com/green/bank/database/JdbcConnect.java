package com.green.bank.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import com.green.bank.util.DatabaseException;

public class JdbcConnect {
	private static Logger logger = Logger.getLogger(JdbcConnect.class);

	private static BasicDataSource ds = new BasicDataSource();
	static {
		Properties p = new Properties();
		try {
			p.load(JdbcConnect.class.getResourceAsStream("/database.properties"));
			ds.setDriverClassName(p.getProperty("jdbc.driver"));
			ds.setUsername(p.getProperty("jdbc.username"));
			ds.setPassword(p.getProperty("jdbc.password"));
			ds.setUrl(p.getProperty("jdbc.url"));
			ds.setMaxTotal(200);
			ds.setMinIdle(50);
			ds.setMaxConnLifetimeMillis(5000);
			ds.setMaxWaitMillis(1000);
			
		} catch (IOException e) {
			logger.error("Unable to read the property file database.properties" + e.getMessage());
		}
	}
	
	private JdbcConnect() {}

	// connection pool
	public static Connection getConnection() throws DatabaseException {
		try {
			return ds.getConnection();
		} catch (SQLException e) {
			throw new DatabaseException("Error obtaining connection: " + e.getMessage());
		}
	}
}
