package com.green.bank.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.green.bank.model.CreateDepositSchemeModel;
import com.green.bank.model.DepositSchemeModel;
import com.green.bank.util.DatabaseException;

public class DepositRepository implements DepositRepositoryInterface {
	
	private Logger logger = Logger.getLogger(DepositRepository.class);
	
	// TODO make transactional
	public DepositSchemeModel createDepositScheme(CreateDepositSchemeModel createModel) throws DatabaseException {
		logger.info("Enter deposit repository to create deposit scheme: " + createModel);

		String accountNo = createModel.getAccountNo();
		
		// Get current date time
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String currentDateTime = dateFormat.format(date);
		
		// Create statement string
		String insertStatementStr = "insert into deposit" 
				+ "(id, year, interest, amount, deposit_date, plan)"
				+ "values(?, ?, ?, ?, ?, ?)";
		
		try (	Connection con = JdbcConnect.getConnection();
				PreparedStatement insertStatement = con.prepareStatement(insertStatementStr); ) {
			
			insertStatement.setString(1, accountNo);
			insertStatement.setInt(2, createModel.getYear());
			insertStatement.setInt(3, createModel.getInterestRate());
			insertStatement.setInt(4, createModel.getAmount());
			insertStatement.setString(5, currentDateTime);
			insertStatement.setString(6, createModel.getValue());
			
			logger.info("Create deposit for account number: " + accountNo);
			int row = insertStatement.executeUpdate();
			
			if (row != 1) {
				String err = "Inserted row is more than one";
				logger.error(err);
				throw new DatabaseException(err);
			}
			
		}	catch (SQLException e) {
			String err = "Error when insert deposit scheme: " + e.getMessage();
			logger.error(err);
			throw new DatabaseException(err);
		}
		
		logger.info("Create deposit scheme success for account number: " + createModel);
		return parse(createModel, currentDateTime);
	}
	
	private DepositSchemeModel parse(CreateDepositSchemeModel createModel, String dateTime) {
		return new DepositSchemeModel(
				createModel.getAccountNo(), 
				dateTime, 
				createModel.getValue(), 
				createModel.getYear(), 
				createModel.getInterestRate(), 
				createModel.getAmount());
	}

}
