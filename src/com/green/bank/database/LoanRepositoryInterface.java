package com.green.bank.database;

import java.util.List;

import com.green.bank.model.CreateLoanModel;
import com.green.bank.model.GetLoanModel;
import com.green.bank.model.LoanModel;
import com.green.bank.util.DatabaseException;

public interface LoanRepositoryInterface {
	
	List<LoanModel> getLoanList(GetLoanModel getModel) throws DatabaseException;
	
	LoanModel createLoan(CreateLoanModel createModel) throws DatabaseException;
}
