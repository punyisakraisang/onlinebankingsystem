package com.green.bank.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.green.bank.database.LoanRepository;
import com.green.bank.database.LoanRepositoryInterface;
import com.green.bank.model.CreateLoanModel;
import com.green.bank.model.GetLoanModel;
import com.green.bank.model.LoanModel;
import com.green.bank.util.DatabaseException;

public class LoanService implements LoanServiceInterface {

	private static LoanRepositoryInterface loanRepo = new LoanRepository();

	private Logger logger = Logger.getLogger(LoanService.class);
	
	public LoanModel createLoan(CreateLoanModel createModel) throws DatabaseException {
		logger.info("Enter loan service to create loan: " + createModel);
		
		// Create loan
		LoanModel model = loanRepo.createLoan(createModel);
		
		logger.info("Create loan success: " + createModel);
		return model;
	}
	

	public List<LoanModel> getLoanList(GetLoanModel getModel) throws DatabaseException {
		logger.info("Enter loan service to get loan: " + getModel);
		
		// Create loan
		List<LoanModel> models = loanRepo.getLoanList(getModel);
		
		logger.info("Get loan success: " + getModel);
		return models;
	}
}
