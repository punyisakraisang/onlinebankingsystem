package com.green.bank.service;

import com.green.bank.model.CreateDepositSchemeModel;
import com.green.bank.model.DepositSchemeModel;
import com.green.bank.util.DatabaseException;

public interface DepositSchemeServiceInterface {
	
	DepositSchemeModel createDepositScheme(CreateDepositSchemeModel createModel) throws DatabaseException;
}
