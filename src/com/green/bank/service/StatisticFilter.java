package com.green.bank.service;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

public class StatisticFilter implements Filter {
	private Logger logger = Logger.getLogger(StatisticFilter.class);

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		long startTime = System.nanoTime();
		
		chain.doFilter(request, response); // go ahead
		
		long elapsedTime = System.nanoTime() - startTime;
		
		logger.info("Time elapsed: " + elapsedTime/1000000 + " ms");
	}

}
