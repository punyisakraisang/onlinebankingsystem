package com.green.bank.service;

import com.green.bank.model.AccountModel;
import com.green.bank.model.CreateAccountModel;
import com.green.bank.model.GetAccountModel;
import com.green.bank.model.TransferModel;
import com.green.bank.model.UpdateModel;
import com.green.bank.util.DatabaseException;

public interface AccountServiceInterface {

	AccountModel getAccount(GetAccountModel getModel) throws DatabaseException;
	
	AccountModel createAccount(CreateAccountModel createModel) throws DatabaseException;
	
	AccountModel deposit(UpdateModel updateModel) throws DatabaseException;
	
	AccountModel withdraw(UpdateModel updateModel) throws DatabaseException;
	
	AccountModel transfer(TransferModel transferModel) throws DatabaseException;
}
