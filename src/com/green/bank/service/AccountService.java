package com.green.bank.service;

import org.apache.log4j.Logger;

import com.green.bank.database.AccountRepository;
import com.green.bank.database.AccountRepositoryInterface;
import com.green.bank.model.AccountModel;
import com.green.bank.model.CreateAccountModel;
import com.green.bank.model.DepositAmountModel;
import com.green.bank.model.GetAccountModel;
import com.green.bank.model.MailModel;
import com.green.bank.model.MailType;
import com.green.bank.model.TransferModel;
import com.green.bank.model.UpdateModel;
import com.green.bank.model.WithdrawAmountModel;
import com.green.bank.util.DatabaseException;

public class AccountService implements AccountServiceInterface {
	
	private static AccountRepositoryInterface accountRepo = new AccountRepository();
	private static MailServiceInterface mailService = new MailService();
	
	private static Logger logger = Logger.getLogger(AccountService.class);

	public AccountModel getAccount(GetAccountModel getModel) throws DatabaseException {
		logger.info("Enter account service to get account: " + getModel);

		AccountModel model = accountRepo.getAccount(getModel);
		
		logger.info("Get account success: " + getModel);
		return model;
	}

	public AccountModel createAccount(CreateAccountModel createModel) throws DatabaseException {
		logger.info("Enter account service to create account with username: " + createModel.getUsername());

		AccountModel model = accountRepo.createAccount(createModel);

		logger.info("Create account success for username: " + model.getUsername() + " account number: " + model.getAccountNo());
		return model;
	}
	
	public AccountModel deposit(UpdateModel updateModel) throws DatabaseException {
		logger.info("Enter account service to deposit: " + updateModel);

		// Verify account number, username, and password
		GetAccountModel getModel = new GetAccountModel(
				updateModel.getAccountNo(), updateModel.getUsername(), updateModel.getPassword());
		logger.info("Retrieve account information: " + getModel);
		accountRepo.getAccount(getModel);
		
		// Make deposit
		DepositAmountModel deposit = new DepositAmountModel(updateModel.getAccountNo(), updateModel.getUpdateAmount());
		logger.info("Deposit to account: " + updateModel.getAccountNo());
		AccountModel model = accountRepo.deposit(deposit);

		logger.info("Deposit success: " + updateModel);
		return model;
	}

	public AccountModel withdraw(UpdateModel updateModel) throws DatabaseException {
		logger.info("Enter account service to withdraw: " + updateModel);

		// Verify account number, username, and password
		GetAccountModel getModel = new GetAccountModel(
				updateModel.getAccountNo(), updateModel.getUsername(), updateModel.getPassword());
		logger.info("Retrieve account information: " + getModel);
		accountRepo.getAccount(getModel);
		
		// Make withdraw
		WithdrawAmountModel withdraw = new WithdrawAmountModel(updateModel.getAccountNo(), updateModel.getUpdateAmount());
		logger.info("Withdraw from account: " + updateModel.getAccountNo());
		AccountModel model = accountRepo.withdraw(withdraw);

		logger.info("Withdraw success: " + updateModel);
		return model;
	}
	
	public AccountModel transfer(TransferModel transferModel) throws DatabaseException {
		logger.info("Enter account service to transfer: " + transferModel);
		
		// Verify account number, username, and password
		GetAccountModel getSenderModel = new GetAccountModel(
				transferModel.getAccountNo(), transferModel.getUsername(), transferModel.getPassword());
		logger.info("Retrieve account information of sender: " + getSenderModel);
		AccountModel model = accountRepo.getAccount(getSenderModel);
		
		// Get email of user for email notification
		String recipient = model.getEmail();
		
		try { // try-catch for email notification purpose
		
			// Verify target account
			GetAccountModel getTargetModel = new GetAccountModel(transferModel.getTargetAccountNo());
			logger.info("Retrieve account information of target: " + getTargetModel);
			accountRepo.getAccount(getTargetModel);
			
			// Prepare request for withdraw and deposit
			String accountNo = transferModel.getAccountNo();
			WithdrawAmountModel withdraw = new WithdrawAmountModel(
					transferModel.getAccountNo(), transferModel.getTransferAmount());
			DepositAmountModel deposit = new DepositAmountModel(
					transferModel.getTargetAccountNo(), transferModel.getTransferAmount());
			
			synchronized(accountNo.intern()) {
				synchronized(transferModel.getTargetAccountNo().intern()) {
					// Make withdraw from sender
					logger.info("Withdraw from sender account: " + withdraw.getAccountNo());
					model = accountRepo.withdraw(withdraw);
					
					// Make deposit to target
					logger.info("Deposit to account: " + transferModel.getTargetAccountNo());
					accountRepo.deposit(deposit);
				}
			}
			
		} catch (DatabaseException e) {
			notifyTransferFailed(recipient, e.getMessage(), transferModel);
			throw e;
		}
		
		logger.info("Transfer success: " + transferModel);
		notifyTransferSuccess(recipient, transferModel);
		return model;
	}
	
	private void notifyTransferSuccess(String recipient, TransferModel model) {
		logger.info("Send email notification to " + recipient);
		MailModel mail = new MailModel(recipient, MailType.TRANSFER_SUCCESS, model);
		mailService.sent(mail);
	}
	
	private void notifyTransferFailed(String recipient, String error, TransferModel model) {
		logger.info("Transfer failed. Send email notification to " + recipient);
		MailModel mail = new MailModel(recipient, MailType.TRANSFER_FAIL, error, model);
		mailService.sent(mail);
	}

}