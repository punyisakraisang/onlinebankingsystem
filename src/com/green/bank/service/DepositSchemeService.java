package com.green.bank.service;

import org.apache.log4j.Logger;

import com.green.bank.database.AccountRepository;
import com.green.bank.database.AccountRepositoryInterface;
import com.green.bank.database.DepositRepository;
import com.green.bank.database.DepositRepositoryInterface;
import com.green.bank.model.CreateDepositSchemeModel;
import com.green.bank.model.DepositSchemeModel;
import com.green.bank.model.WithdrawAmountModel;
import com.green.bank.util.DatabaseException;

public class DepositSchemeService implements DepositSchemeServiceInterface {

	private static AccountRepositoryInterface accountRepo = new AccountRepository();
	private static DepositRepositoryInterface depositRepo = new DepositRepository();

	private Logger logger = Logger.getLogger(DepositSchemeService.class);
	
	// TODO: transactional
	public DepositSchemeModel createDepositScheme(CreateDepositSchemeModel createModel) throws DatabaseException {
		logger.info("Enter deposit scheme service to create deposit: " + createModel);
		
		// Withdraw
		logger.info("Withdraw money from account: " + createModel.getAccountNo());
		WithdrawAmountModel withdraw = new WithdrawAmountModel(createModel.getAccountNo(), createModel.getAmount());
		accountRepo.withdraw(withdraw);
		
		// Create deposit scheme
		logger.info("Create deposit for account: " + createModel.getAccountNo());
		DepositSchemeModel model = depositRepo.createDepositScheme(createModel);
		
		logger.info("Create deposit scheme success for account: " + createModel);
		return model;
	}
}
