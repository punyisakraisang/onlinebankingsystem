package com.green.bank.service;

import java.io.InputStream;
import java.time.LocalDateTime;

import org.apache.log4j.Logger;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.green.bank.model.FileModel;
import com.green.bank.util.ClientException;
import com.green.bank.util.S3Util;

public class FileService implements FileServiceInterface {
	
	private final Logger logger = Logger.getLogger(FileService.class);

	public String upload(FileModel model) throws ClientException {
		logger.info("Enter file service to upload file for: " + model.getServiceName());
		
		String urlStr = "";
		InputStream fileContent = model.getFileContent();
		
		// Build file name
		String fileName = model.getServiceName() 
				+ "_" + model.getFirstName().toLowerCase() 
				+ model.getLastName().toLowerCase()
				+ getDateString()
				+ "." + model.getExtension();
		
		try {
            AmazonS3 s3Client = S3Util.getClient();
            PutObjectRequest putRequest = new PutObjectRequest(S3Util.BUCKET_NAME, fileName, fileContent, S3Util.getPdfMeta())
            		.withCannedAcl(CannedAccessControlList.PublicRead);
            
    		logger.info("Upload file to s3: " + fileName);
            s3Client.putObject(putRequest);
            
    		logger.info("Get uploaded file's url: " + fileName);
    		urlStr = s3Client.getUrl(S3Util.BUCKET_NAME, fileName).toString();
            
        } catch (SdkClientException e) {
			logger.error("Error when upload document to cloud: " + e.getMessage());
			throw new ClientException(e.getMessage());
        }
		
		logger.info("Upload file successfully for: " + model.getServiceName() + " fileName:" + fileName);
		return urlStr;
	}
	
	private String getDateString() {
		return LocalDateTime.now().toString().replaceAll("\\W", "");
	}
}
