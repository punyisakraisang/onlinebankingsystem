package com.green.bank.service;

import com.green.bank.model.FileModel;
import com.green.bank.util.ClientException;

public interface FileServiceInterface {

	String upload(FileModel model) throws ClientException;
}
