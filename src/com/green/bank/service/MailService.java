package com.green.bank.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.green.bank.model.MailModel;
import com.green.bank.model.TransferModel;
import com.green.bank.util.MailUtil;

public class MailService implements MailServiceInterface {
	
	private static final ExecutorService exec = Executors.newFixedThreadPool(4);
	
	private final Logger logger = Logger.getLogger(MailService.class);
	
	public void sent(MailModel mailModel) {
		logger.info("Enter mail service to send email: " + mailModel);
		
		// Create new task to send the mail
		Runnable task = () -> sentAsync(mailModel);
		
		logger.info("Assign thread to send email: " + mailModel);
		exec.execute(task);

	}
	
	private void sentAsync(MailModel mailModel) {
		logger.info("Start sent mail in background: " + mailModel);

		switch(mailModel.getType()) {
		case TRANSFER_SUCCESS:
			MailUtil.send(
					mailModel.getRecipient(), 
					"Cha Ching!", 
					getSuccessMessage(mailModel.getModel()));
			break;
			
		case TRANSFER_FAIL:
			MailUtil.send(
					mailModel.getRecipient(), 
					"Ooops!", 
					getFailMessage(mailModel.getErrorMessage(), mailModel.getModel()));
			break;
		default:
			break;
			
		}

		logger.info("Finish sent mail in background: " + mailModel);
	}
	
	private String getSuccessMessage(TransferModel model) {
		StringBuilder builder = new StringBuilder();
		builder.append("Your transfer of ");
		builder.append(model.getTransferAmount());
		builder.append(" from Green Bank Account ");
		builder.append(model.getAccountNo());
		builder.append(" to ");
		builder.append(model.getTargetAccountNo());
		builder.append(" is success! ");
		return builder.toString();
	}
	
	private String getFailMessage(String error, TransferModel model) {
		StringBuilder builder = new StringBuilder();
		builder.append("Your transfer of ");
		builder.append(model.getTransferAmount());
		builder.append(" from Green Bank Account ");
		builder.append(model.getAccountNo());
		builder.append(" to ");
		builder.append(model.getTargetAccountNo());
		builder.append(" is failed! ");
		builder.append(error);
		return builder.toString();
	}
	
	@Override
	protected void finalize() throws Throwable {
		exec.shutdown();
		super.finalize();
	}
}
