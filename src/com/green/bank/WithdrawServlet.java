package com.green.bank;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.green.bank.model.AccountModel;
import com.green.bank.model.CustomError;
import com.green.bank.model.UpdateModel;
import com.green.bank.service.AccountService;
import com.green.bank.service.AccountServiceInterface;
import com.green.bank.util.DatabaseException;
import com.green.bank.util.ValidateUtil;

public class WithdrawServlet extends HttpServlet {
	
	private static final long serialVersionUID = -8116349373341007484L;
	
	private final AccountServiceInterface repo = new AccountService();
	
	private final Logger logger = Logger.getLogger(WithdrawServlet.class);
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		logger.info("Enter WithdrawServlet");
		
		if(!ValidateUtil.validateWithdrawRequest(request)) {
			logger.error(CustomError.INVALID_PARAMETER);
			request.setAttribute("error", CustomError.INVALID_PARAMETER.toString());
			goBack(request, response);
			return;
		}
		
		String accountNo = request.getParameter("account_no");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		int withdrawAmount = Integer.parseInt(request.getParameter("amount")); 

		try {
			UpdateModel updateModel = new UpdateModel(accountNo, username, password, withdrawAmount);

			logger.info("Withdraw from account: " + accountNo);
			AccountModel model = repo.withdraw(updateModel);
			
			// Save current account details in session
			request.getSession().setAttribute("userDetails", model);
			
			logger.info("Withdraw success for account: " + accountNo);
			request.getRequestDispatcher("Withdraw_process.jsp").forward(request, response);
			
		} catch (DatabaseException e) {
			logger.error("Error when withdraw amount: " + e.getMessage());
			request.setAttribute("error", e.getMessage());
			goBack(request, response);
		}
		
	}

	private void goBack(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("withdraw.jsp").forward(request, response);
	}
}
