<%@ page import="com.green.bank.model.AccountModel"%>
<%@ page import="com.green.bank.model.GetAccountModel"%>
<%@ page import="com.green.bank.database.AccountRepository"%>
<%@ page import="com.green.bank.database.AccountRepositoryInterface"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>

<%! 
	// Global variable
	AccountRepositoryInterface repo = new AccountRepository();
%>

<%
	AccountModel ac = (AccountModel) session.getAttribute("userDetails");
	GetAccountModel getModel = new GetAccountModel(ac.getAccountNo());
	
	// Get account detail for latest information
	ac = repo.getAccount(getModel);
%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=ac.getFirstName() + " " + ac.getLastName()%></title>
<link rel="shortcut icon" type="image/png" href="image/favicon.png" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/profile.css" rel="stylesheet">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="row">
		<jsp:include page="header.jsp" />
	</div>

	<div class="container-fullwidth">
		<div class="jumbotron col-md-6 col-md-offset-3"
			style="margin-top: 50px">
			<div class="row">
				<div class="profile-head col-md-10 col-md-offset-1">
					<div class="col-md-4 ">
						<img class="img-circle img-responsive" alt="" src="image/user.png">
					</div>


					<div class="col-md-6 ">
						<h2><%=ac.getFirstName() + " " + ac.getLastName()%></h2>
						<ul>
							<li class="navli"><span
								class="glyphicon glyphicon-map-marker"></span> <%=ac.getBranch()%></li>
							<li class="navli"><span class="glyphicon glyphicon-home"></span>
								<%=ac.getAddress()%></li>
							<li class="navli"><span class="glyphicon glyphicon-phone"></span><%=ac.getPhoneNumber()%></li>
							<li class="navli"><span class="glyphicon glyphicon-envelope"></span><%=ac.getEmail()%></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="account_details col-md-10 col-md-offset-1"
					style="margin-top: 30px;">
					<h2>Account Details</h2>
					<hr class="divider">
					<table class="table table-user-information col-md-6" summary="account details">
						<tbody>
							<tr>
								<th scope="row">First Name:</th>
								<td><%=ac.getFirstName()%></td>
							</tr>
							<tr>
								<th scope="row">Last Name:</th>
								<td><%=ac.getLastName()%></td>
							</tr>
							<tr>
								<th scope="row">Account Number:</th>
								<td><%=ac.getAccountNo()%></td>
							</tr>
							<tr>
								<th scope="row">City</th>
								<td><%=ac.getCity()%></td>
							</tr>
							<tr>
								<th scope="row">Branch Name</th>
								<td><%=ac.getBranch()%></td>
							</tr>
							<tr>
								<th scope="row">Zip</th>
								<td><%=ac.getZip()%></td>
							</tr>
							<tr>
								<th scope="row">UserName</th>
								<td><%=ac.getUsername()%></td>
							</tr>
							<tr>
								<th scope="row">Phone Number</th>
								<td><%=ac.getPhoneNumber()%></td>
							</tr>
							<tr>
								<th scope="row">Email</th>
								<td><a href="mailto:" +<%=ac.getEmail()%>><%=ac.getEmail()%></a></td>
							</tr>
							<tr>
								<th scope="row">Account Type</th>
								<td><%=ac.getAccountType()%></td>
							</tr>
							<tr>
								<th scope="row">Registration Date</th>
								<td><%=ac.getRegDate()%></td>
							</tr>
							<tr>
								<th scope="row">Amount</th>
								<td><%=ac.getAmount()%>&#2547;</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="row"></div>

		<!-- Footer start here -->
		<div class="row" style="margin-top: 50px;">
			<jsp:include page="footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>