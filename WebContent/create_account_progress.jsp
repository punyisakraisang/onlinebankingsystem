<%@page import="com.green.bank.service.AccountService"%>
<%@page import="com.green.bank.service.AccountServiceInterface"%>
<%@page import="com.green.bank.model.AccountModel"%>
<%@page import="com.green.bank.model.CreateAccountModel"%>
<%@page import="com.green.bank.util.DatabaseException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Account</title>
<link rel="shortcut icon" type="image/png" href="image/favicon.png" />
<link rel="stylesheet" type="text/css" href="css/deposit.css">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="row">
		<jsp:include page="header.jsp" />
	</div>

	<!-- Declaring all variables -->
	<%!
		AccountModel model = null;
		CreateAccountModel createModel = null;
		AccountServiceInterface service = new AccountService();
	%>

	<!-- Inserting account info into Database -->
	<%
		createModel = (CreateAccountModel) request.getAttribute("Account_details");
		try {
			model = service.createAccount(createModel);
	%>

	<div class="container-fullwidth">
		<div class="row" style="margin-top: 50px;">
			<div class="alert alert-success col-md-4 col-md-offset-4" role="alert">
				<h4 class="alert-heading">Success!</h4>
				<p>
					<strong>Your Account has been created.</strong>
				</p>
				<p class="mb-0">
					<strong>Account Number: </strong>
					<%=model.getAccountNo()%>
				</p>
				<p class="mb-0">
					<strong>Amount: </strong>
					<%=model.getAmount()%>
				</p>
			</div>
		</div>
		<%
		} catch (DatabaseException e) {
		%>
		<div class="container-fullwidth">
			<div class="row" style="margin-top: 50px;">
				<div class="alert alert-danger col-md-4 col-md-offset-4" role="alert">
					<strong>Oh snap!</strong> <%= e.getMessage() %>
				</div>
			</div>
		</div>

		<%
			}
		%>
		<!-- Footer start here -->
		<div class="row" style="margin-top: 50px;">
			<jsp:include page="footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>