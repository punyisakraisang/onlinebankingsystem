<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Footer</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />
	
	<link rel="stylesheet" href="css/footer.css">
	
	<link href="http://fonts.googleapis.com/css?family=Cookie"
		rel="stylesheet" type="text/css">
	
	<link rel="stylesheet"
		href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	

</head>

<body>
	<footer class="footer-distributed">
	
		<div class="footer-left">
			<div class="row" style="margin-top: 50px;">
				<div class="image">
	
					<img src="image/greenbank.png" alt="" />
					<p class="image_text">Honors your trust</p>
	
				</div>
			</div>
		</div>
	
		<div class="footer-center">
			<div>
				<i class="fa fa-map-marker"></i>
				<p>
					<span><strong>Head office:</strong>&nbsp;&nbsp;Road-11,Block-D, Banani,
						Dhaka-1213, Bangladesh</span>
				</p>
			</div>
	
			<div>
				<i class="fa fa-phone"></i>
				<p>
					<span>941-752-5305</span>
				</p>
			</div>
	
			<div>
				<i class="fa fa-envelope"></i>
				<p>
					<span>contact@greenbank.com</span>
				</p>
			</div>
	
			<div>
				<i class="fa fa-mobile"></i>
				<p>
					<span>(+880) 123456789</span>
				</p>
	
			</div>
	
		</div>
	
		<div class="footer-right">
	
			<div class="footer-company-about">
				<div class="row">
					<span class="hidden-xs"> 
						<h3>Our Mission Statement:</h3>
						<h4>
							<font color="white">
								To deliver great personal service and
								the best financial products to our customers and the local
								communities we serve.
							</font>
						</h4>
					</span>
				</div>
	
				<div class="footer-icons">
					<font color="white">
						<a href="#" target="_blank"><i class="fa fa-facebook" /></a> 
						<a href="#" target="_blank"><i class="fa fa-twitter" /></a> 
						<a href="#" target="_blank"><i class="fa fa-instagram" /></a> 
						<a href="#" target="_blank"><i class="fa fa-pinterest-square" /></a>
						<a href="#" target="_blank"><i class="fa fa-linkedin" /></a> 
						<a href="#" target="_blank"><i class="fa fa-youtube" /> </a> 
					</font>
				</div>
			</div>
		</div>
				
	</footer>

</body>

</html>