<%@page import="com.green.bank.model.LoanModel"%>
<%@page import="com.green.bank.model.GetLoanModel"%>
<%@page import="com.green.bank.service.LoanService"%>
<%@page import="com.green.bank.service.LoanServiceInterface"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Loan Request List</title>
<link rel="shortcut icon" type="image/png" href="image/favicon.png" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<%! 
		LoanServiceInterface loanService = new LoanService(); 
	%>
	<%
		GetLoanModel getModel = new GetLoanModel("pending");
		List<LoanModel> loadList = loanService.getLoanList(getModel);
	%>

	<div class="row">
		<jsp:include page="header.jsp" />
	</div>
	<div class="container-fullwidth">
		<div class="row" style="margin-top: 50px;">
			<div class="col-md-8 col-md-offset-2">
				<h3>Loan Requests</h3>
				<div class="table-responsive" style="margin-top: 40px">


					<table id="mytable" class="table table-bordred table-striped" summary="loan request">

						<thead>
							<th scope="col">First Name</th>
							<th scope="col">Last Name</th>
							<th scope="col">Account No</th>
							<th scope="col">Address</th>
							<th scope="col">Email</th>
							<th scope="col">Amount</th>
							<th scope="col">Approve</th>
							<th scope="col">Delete</th>
						</thead>
						<tbody>
							<%
								for (LoanModel l : loadList) {
							%>
							<tr>
								<td><%=l.getFirstName()%></td>
								<td><%=l.getLastName()%></td>
								<td><%=l.getAccountNo()%></td>
								<td><%=l.getAddress()%></td>
								<td><%=l.getEmail()%></td>
								<td><%=l.getLoanAmount()%></td>
								<td><p data-placement="top" data-toggle="tooltip"
										title="Approve">
										<a
											href="loan_request_process.jsp?account_no=<%=l.getAccountNo()%>&amount=<%=l.getLoanAmount()%>"><button
												class="btn btn-primary btn-sm" data-title="Approve"
												data-toggle="modal" data-target="#edit">
												<span class="glyphicon glyphicon-ok"></span>
											</button></a>
									</p></td>
								<td><p data-placement="top" data-toggle="tooltip"
										title="Delete">
										<button class="btn btn-danger btn-sm" data-title="Delete"
											data-toggle="modal" data-target="#delete">
											<span class="glyphicon glyphicon-trash"></span>
										</button>
									</p></td>
							</tr>
							<%
								}
							%>

						</tbody>

					</table>


				</div>

			</div>
		</div>

		<!-- Footer start here -->
		<div class="row" style="margin-top: 50px;">
			<jsp:include page="footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>